require 'test_helper'

class DoadorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @doador = doadors(:one)
  end

  test "should get index" do
    get doadors_url
    assert_response :success
  end

  test "should get new" do
    get new_doador_url
    assert_response :success
  end

  test "should create doador" do
    assert_difference('Doador.count') do
      post doadors_url, params: { doador: { cpf: @doador.cpf, email: @doador.email, endereco: @doador.endereco, idade: @doador.idade, nome: @doador.nome, telefone: @doador.telefone, tipo_sanguineo: @doador.tipo_sanguineo } }
    end

    assert_redirected_to doador_url(Doador.last)
  end

  test "should show doador" do
    get doador_url(@doador)
    assert_response :success
  end

  test "should get edit" do
    get edit_doador_url(@doador)
    assert_response :success
  end

  test "should update doador" do
    patch doador_url(@doador), params: { doador: { cpf: @doador.cpf, email: @doador.email, endereco: @doador.endereco, idade: @doador.idade, nome: @doador.nome, telefone: @doador.telefone, tipo_sanguineo: @doador.tipo_sanguineo } }
    assert_redirected_to doador_url(@doador)
  end

  test "should destroy doador" do
    assert_difference('Doador.count', -1) do
      delete doador_url(@doador)
    end

    assert_redirected_to doadors_url
  end
end
