class CreateSangues < ActiveRecord::Migration[5.1]
  def change
    create_table :sangues do |t|
      t.string :tipo
      t.string :quantidade

      t.timestamps
    end
  end
end
