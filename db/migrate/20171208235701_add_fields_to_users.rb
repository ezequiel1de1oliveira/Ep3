class AddFieldsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :idade, :integer
    add_column :users, :telefone, :string
    add_column :users, :endereco, :string
    add_column :users, :cpf, :string
    add_column :users, :tipo_sanguineo, :string

  end
end
