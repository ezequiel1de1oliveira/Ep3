json.extract! doador, :id, :nome, :idade, :telefone, :email, :cpf, :endereco, :tipo_sanguineo, :created_at, :updated_at
json.url doador_url(doador, format: :json)
